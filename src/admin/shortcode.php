<?php

function tlbx_mindmap_scripts()
{

  wp_register_style('tlbx_mindmap_css', plugins_url('../../style.css', __FILE__));
  wp_enqueue_style('tlbx_mindmap_css');

  wp_register_script('tlbx_mindmap_js', plugins_url('../js/main_v3.js', __FILE__), array('d3_js'));
  wp_enqueue_script('tlbx_mindmap_js');

 //wp_enqueue_script('d3', 'https://cdnjs.cloudflare.com/ajax/libs/d3/4.6.0/d3.min.js', array('jquery'));
  wp_register_script('d3_js', plugins_url('../js/vendor/d3.v3.min.js', __FILE__), array('jquery'));
 // wp_register_script('d3_js', plugins_url('d3.v4.min.js', __FILE__), array('jquery'));
  wp_enqueue_script('d3_js');
  wp_localize_script('tlbx_mindmap_js', 'MyAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
}



add_shortcode("inklusion-mindmap", "inklusionmindmap_handler");
function inklusionmindmap_handler()
{
  $nodes = array();
  $i = 0;
  if (have_rows('elements', "option")) {
    while (have_rows('elements', "option")) {
      the_row();
      $n = get_sub_field('name', "option");
      $o = get_sub_field('group', "option");
      $d = get_sub_field('beschreibung', "option");
      $nodes[] = [
        "id" => $i,
        "name" => $n,
        "group" => $o['label'],
        "color" => $o['value'],
        "description" => $d
      ];
      $i++;
    }
  }
  $links = array();
  if (have_rows('links', "option")) {
    while (have_rows('links', "option")) {
      the_row();
      $source = get_sub_field('element_a', "option");
      $target = get_sub_field('element_b', "option");
    //  var_dump( $source );
      if ($source >= 0 && $target >= 0) {
       // echo  $source ." -> ".$target;

        $links[] = [
          "source" => intval($source),
          "target" => intval($target)
        ];
      }
    }
  }
  ?>


          <script>
            var jsdataPass = <?php echo json_encode(array('nodes' => $nodes, 'links' => $links), JSON_UNESCAPED_UNICODE); ?>;
          </script>
        <?php

        add_action('wp_footer', 'tlbx_mindmap_scripts');
        return '<div id="mind_map"></div>';
      }
      ?>
