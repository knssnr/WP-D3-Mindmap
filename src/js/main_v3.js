jQuery(document).ready(function ($) {
  var width_container = $('#mind_map').width();
  //var height_container = document.getElementById('mind_map').offsetHeight;
  var margin = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    },
    width = width_container - margin.left - margin.right,
    height = width_container - margin.top - margin.bottom,
    radius = width / 2,
    diameter = width,
    diagonal = d3.svg.diagonal.radial().projection(function (d) {
      return [d.y, d.x / 180 * Math.PI];
    }),
    tree = d3.layout.tree().size([
      360, diameter / 2 - diameter / 12
    ]);

 /*  $.ajax({
    url: MyAjax.ajaxurl,
    data: {
      action: 'categorytree',
      taxonomy: 'taxonomy'
    },
    success: function (data) {
     // console.log('success ', data);

    },
    error: function (errorThrown) {
      console.log('error ', errorThrown);
    }
  }); */
  var data = jsdataPass;
  //    console.log('success ', data);
  var root = child_hiearachic(data.nodes, data.links);
      root.x0 = height / 2;
      root.y0 = 0;
      radial_cluster(root, data.nodes, data.links);

  /**
   *
   *
   * @param {*} root
   */
  function radial_cluster(root, nodles, lines) {

    console.log("radial cluster loading");
    var root = root;
    d3.select('#mind_map').html('');
    var svg = d3.select('#mind_map').append('svg').attr('width', width).attr('height', width).attr('class', 'chart').attr('viewBox', '0 0 ' + diameter + ' ' + diameter).attr('preserveAspectRatio', 'xMinYMin').append('g').attr('transform', 'translate(' + radius + ',' + radius + ')');

    function update(root) {
      var nodes = tree.nodes(root).reverse();
      svg.selectAll('path.link').remove();
      svg.selectAll('g.node').remove();
      var link = svg.selectAll('path.link').data(tree.links(nodes)).enter().append('path').attr('class', 'link').attr('d', diagonal);

      var node = svg.selectAll('g.node').data(nodes).enter().append('g').attr('class', 'node')
        .attr('transform', function (d) {
          return 'rotate(' + (d.x - 90) + ')translate(' + d.y + ')';
        }).on('click', click);

      node.append('rect')
        .attr("class", "rect")
        .attr("x", -100)
        .attr("y", -50)
        .attr("height", 100)
        .attr("width", 200)
        .style('fill', function (d) {
          return "none"
        }).style('stroke', function (d) {
          return d.color;
        }).attr('transform', function (d) {
          //    return 'rotate(' + (d.x - 90) * -1 + ')translate(' + (d.y / -1)  + ')';
          //    return 'rotate(' + (d.x - 90) * -1 + ')translate(-100,-50)';
        });

      node.append('circle')
        .attr('r', 30.5)
        .style('fill', function (d) {
          return d.color;
        }).append("svg:title")
        .text(
          function(d) { return d.description; }
          );

      node.append('text').attr('dy', '.31em').attr('text-anchor', function (d) {
        //   return d.x < 180 ? "start" : "end";
        return "middle";
      }).attr('transform', function (d) {
        //    return 'rotate(' + (d.x - 90) * -1 + ')translate(' + (d.y / -1)  + ')';
        return 'rotate(' + (d.x - 90) * -1 + ')translate(0)';
      }).text(function (d) {
        return d.name;
      });
    }
    update(root);

    function collapse(d) {
      if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
      }
    }
    //root.children.forEach(collapse);

    update(root);
    // Toggle children on click.
    function click(d) {
      if (d.children) {
        d._children = d.children;
        d.children = null;
      } else {
        d.children = d._children;
        d._children = null;
      }
      update(root);
    }
  }

  function elbow(d, i) {
    return 'M' + d.source.y + ',' + d.source.x +
      'H' + d.target.y + 'V' + d.target.x + (d.target.children ? '' : 'h' + margin.right);
  }

  function child_hiearachic(nodes, links) {
    var root = [];
    for (var i = 0; i < links.length; i += 1) {
      var source_id = links[i].source;
      var target_id = links[i].target;
      var parent = nodes[target_id];
      var child = nodes[source_id];
      if (!parent.children) {
        parent['children'] = [];
      }
      parent.children.push(child);
      if (!child.parent) {
        child['parent'] = [];
      }
      child.parent.push(parent);
      if (parent.id === 0) {
        parent['level'] = '1';
        parent['root'] = true
        root = parent;
      }
    }

    return root;
  }
});

/*
Exception: ReferenceError: jQuery is not defined
@Scratchpad/3:1:1
*/
