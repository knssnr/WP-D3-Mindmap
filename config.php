<?php
/*
Plugin Name:  Toolbox Inklusion
Plugin URI:   https://gitlab.com/kniessner/WP-D3-Mindmap
Description:  a custom plugin
Version:      2018.0.1
Author:       Knießner, Sascha-Darius
Author URI:   https://kniessner.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages
*/

/*******************************
* Plugin activation
*******************************/
//include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

function tlbx_mindmap_plugin_install() {
// checks version of wordpress and deactives if lower than 3.1
// check for plugin using plugin name
/*     if ( !is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
      deactivate_plugins(basename(__FILE__)); // Deactiviere das Plugin
      wp_die( 'Advancedcustomfields nicht gefunden' );
    }  */

 /*    if (version_compare( get_bloginfo('version'), '4.5', '<')) {
        deactivate_plugins(basename(__FILE__)); // Deactiviere das Plugin
		    wp_die( 'Deine Wordpress Version muss > 4.5 sein' );
    }  */
}
//register_activation_hook(__FILE__, 'complex_mindmap_plugin_install');



foreach (glob(plugin_dir_path(__FILE__) . "src/admin/*.php") as $file) {
  include_once $file;
}






?>
